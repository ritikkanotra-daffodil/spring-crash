package com.ritik.springcrash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCrashApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCrashApplication.class, args);
	}

}
