package com.ritik.springcrash.repository;

import com.ritik.springcrash.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> { // <Entity Type, PrimaryKey Type>

    public Department findByDepartmentName(String departmentName);

    public List<Department> findByDepartmentNameIgnoreCase(String departmentName);

}
