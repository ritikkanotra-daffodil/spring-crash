package com.ritik.springcrash.service;

import com.ritik.springcrash.entity.Department;
import com.ritik.springcrash.error.DepartmentNotFoundException;

import java.util.List;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public List<Department> fetchDepartmentByName(String departmentName);

}
