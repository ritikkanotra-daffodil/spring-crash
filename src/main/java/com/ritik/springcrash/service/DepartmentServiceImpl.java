package com.ritik.springcrash.service;

import com.ritik.springcrash.entity.Department;
import com.ritik.springcrash.error.DepartmentNotFoundException;
import com.ritik.springcrash.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService{

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public List<Department> fetchDepartmentList() {
        return departmentRepository.findAll();
    }

    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException {
         Optional<Department> department = departmentRepository.findById(departmentId);
         if (!department.isPresent()) {
             throw new DepartmentNotFoundException("Department Not Available");
         }
         return department.get();
    }

    @Override
    public void deleteDepartmentById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }

    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department depDb = departmentRepository.findById(departmentId).get();

        if (department.getDepartmentCode() != null && !department.getDepartmentCode().equals("")) {
            depDb.setDepartmentCode(department.getDepartmentCode());
        }

        if (department.getDepartmentName() != null && !department.getDepartmentName().equals("")) {
            depDb.setDepartmentName(department.getDepartmentName());
        }

        if (department.getDepartmentAddress() != null && !department.getDepartmentAddress().equals("")) {
            depDb.setDepartmentAddress(department.getDepartmentAddress());
        }

        return departmentRepository.save(depDb);

    }

    @Override
    public List<Department> fetchDepartmentByName(String departmentName) {
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }
}