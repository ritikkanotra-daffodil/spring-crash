package com.ritik.springcrash.repository;

import com.ritik.springcrash.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DepartmentRepositoryTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {

        Department department =
                Department.builder()
                        .departmentName("ME")
                        .departmentCode("ME-01")
                        .departmentAddress("Delhi")
                        .build();

        entityManager.persist(department); //putting data into mock database
    }

    @Test
    public void whenFindById_thenReturnDepartment() {
        Department found = departmentRepository.findById(1L).get();
        assertEquals(found.getDepartmentName(), "ME");
    }
}